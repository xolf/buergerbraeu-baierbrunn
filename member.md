---
bg: "together.jpg"
layout: page
title: "Mitglied werden"
crawlertitle: "Mitglied werden"
permalink: /member/
summary: "Unterstützen Sie die lokale Ortsbrauerei"
active: about
---

Es gibt nichts schöneres unter dem bayrischen Himmel, als nach getaner Arbeit sein eigenes Bier zu zapfen.

Kommen Sie auch in den Genuss und werden sie Mitglied in unserer lokalen Ortsbrauerei.
