---
bg: "kegs.jpg"
layout: page
permalink: /posts/
title: "Unsere Sorten"
crawlertitle: "Alle unsere Sorten"
summary: "Für jeden Geschmack ist was dabei"
active: archive
---

{% for tag in site.tags %}
  {% assign t = tag | first %}
  {% assign posts = tag | last %}

  <h2 class="category-key" id="{{ t | downcase }}">{{ t | capitalize }}</h2>

  <ul class="year">
    {% for post in posts %}
      {% if post.tags contains t %}
        <li>
          {% if post.lastmod %}
            <a href="{{ post.url | relative_url}}">{{ post.title }}</a>
          {% else %}
            <a href="{{ post.url | relative_url}}">{{ post.title }}</a>
          {% endif %}
        </li>
      {% endif %}
    {% endfor %}
  </ul>

{% endfor %}
