---
bg: "team.jpg"
layout: page
title: "Über uns"
crawlertitle: "Über uns"
permalink: /about/
summary: "Wieso wir uns für einen Bürgerbräu in Baierbrunn einsetzen"
active: about
---

Mit Rohstoffen aus und von Baierbrunn für Baiebrunner streben wir einen **Bürgerbräu mit hoher Qualität** an.

Nach dem Vorbild Oberhachings 2015 gegründeten Brauereigenossenschaft setzen wir uns mittelfristig proaktiv für die umweltfreundliche Bewerbeansiedlung am Ort ein.
